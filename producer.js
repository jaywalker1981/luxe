var querystring = require('querystring');
var http = require('http');
var sleep = require('sleep');
var MAX = 10;
var host = process.env.omer_host || "127.0.0.1";
var port = process.env.omer_port || 8081;
var SLA_factor = process.env.SLA_factor || 5;
var request = require('sync-request');


function newTask() {

    var responseString = '';
    var duration = Math.round(Math.random() * MAX);
    var obj = {"duration" : duration, "SLA": duration * SLA_factor };
    var body = JSON.stringify(obj);
    console.log(body);

    var res = request('POST', "http://" + host + ":" + port + "/" + "Tasks", {
	    json: JSON.parse(body)
	});
}

while(true)
{

    sleep.sleep(Math.round(Math.random() * 4));
    newTask();

}